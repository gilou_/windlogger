




`$1+entrée` Menu général (General)
```console
*11 Configurer de l'échantillonnage (mesure sample conf)
*12 Donner un identifiant au datalogger compris entre 0 et 255 (node id)
*19 Remise à zéro de tous les paramètres généraux, sauf la date et l'heure (reset configuration)
```
`$2+entrée` Date et heure (Date/Time)
```console
*21 Heure (Time) sous la forme xx:xx:xx
*22 Date, sous la forme MM/JJ/AAAA
```
`$3+entrée` Paramètres de l'anémomètre 1 (Anemo1)
```console
*31 Activer ou désactiver le capteur (Enable)
*32 Appliquer un facteur à la mesure (pour convertir d'une unité à une autre par exemple) (Factor)
*33 Appliquer un offset (=valeur à l'origine) à la mesure (Offset)
```
`$4+entrée` Paramètres de l'anémomètre 2 (Anemo2)
```console
*41 Activer ou désactiver le capteur (Enable)
*42 Appliquer un facteur à la mesure (pour convertir d'une unité à une autre par exemple) (Factor)
*43 Appliquer un offset (=valeur à l'origine) à la mesure (Offset)
```
`$5+entrée` Paramètres de la girouette (Windvane)
```console
*51 Activer ou désactiver le capteur (Enable)
*52 Appliquer un facteur à la mesure (pour convertir d'une unité à une autre par exemple) (Factor)
*53 Appliquer un offset (=valeur à l'origine) à la mesure (Offset)
```
`$6+entrée` Paramètres des mesures de puissance 1 (Power1)
```console
*61 Activer ou désactiver le capteur (Enable)
*62 Choisir une mesure alternative ou continue (AC Power)
*63 Appliquer un facteur à la sonde de tension (voltage factor)
*64 Appliquer un offset (=valeur à l'origine) à la mesure de tension (voltage offset)
*65 Appliquer un facteur pour régler le déphasage entre la tension et l'intensité (voltage phase)
*66 Appliquer un facteur à la sonde de courant (current factor)
*67 Appliquer un offset (=valeur à l'origine) à la mesure de courant (curent offset)
*69 Choisir la précision des mesures (Data accuracy)
```
`$7+entrée` Paramètres des mesures de puissance 2 (Power2)
```console
*71 Activer ou désactiver le capteur (Enable)
*72 > choisir une mesure alternative ou continue (AC Power)
*73 Appliquer un facteur à la sonde de tension (voltage factor)
*74 Appliquer un offset (=valeur à l'origine) à la mesure de tension (voltage offset)
*75 Appliquer un facteur pour régler le déphasage entre la tension et l'intensité (voltage phase)
*76 Appliquer un facteur à la sonde de courant (current factor)
*77 Appliquer un offset (=valeur à l'origine) à la mesure de courant (curent offset)
*79 Choisir la précision des mesures (Data accuracy)
```
`$9+entrée` Configuration des sorties (Output configuration)
```console
*91 Activer ou désactiver l'envoi des données sur le port USB (Serial enable)
*92 Activer ou désactiver l'envoi des données sur la carte SD (Sd card enable)
```
### Le calibrage des capteurs

Pour calibrer chaque capteur, suivre les instructions sur [la page wiki de la carte shield](https://gitlab.com/gilou_/windlogger_shield/wikis/Home_FR).


## La transmission et l'enregistrement des données

### Carte USB

### Carte SD

### Wifi - en cours de développement


### Carte SIM (GPRS) - en cours de développement


## Visualisation des données - en cours de développement
Nécessite compétences en informatiques !

## La maintenance

## Les outils de travail collaboratif
On a besoin de vous ! Compétences..

### Gitlab
Fork
Travail et commit réguliers + push
Merge request

Projects > Explore projects

Présentation du projet et installation du datalogger : (Aurélie)
https://gitlab.com/gilou_/windlogger

Partie numérique : (Gilou)
https://gitlab.com/gilou_/windlogger_digital

Partie analogique avec tous les capteurs : (Gilou)
https://gitlab.com/gilou_/windlogger_shield

Code du micro contrôleur : (Luiz)
https://gitlab.com/gilou_/windlogger_firmware

#### Librairies arduino créées pour le projet

Capteur de puissance ?
https://gitlab.com/gilou_/Power

Anémomètre
https://gitlab.com/gilou_/Anemometer

Girouette
https://gitlab.com/gilou_/Windvane

### Atom
Dans votre terminal, installez le programme gdebi :
```console
sudo apt-get install gdebi
```
Téléchargez le logiciel [Atom](https://atom.io/) (fichier .deb)

Dans votre terminal, tapez :
```console
sudo gdebi atom-amd64.deb
```
