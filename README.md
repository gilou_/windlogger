## The Windlogger Project

The windlogger project is an open-source data logger for small wind turbine. That idea come from discussion between Tripalium's members in 2014. Current tools were or expansive or no adapted for measuring what we want. Prototype presentation during WEAthens2014's conferencies, organized by the WindEmpowerment's network confirmed the necessity of this initiative.

The goal of this project is to develop a cheap tool to monitor a wind turbine and to do wind measurement campaign. Data can be stored on SD card or send by USB or Wifi or GPRS.

Actually the availlable wind measurement are not adapted to the small windturbine's world because data are taken at 50 and 80m height. Moreover, The cost of a measuring wind for a small wind turbine is prohibitive.

An other motivation is, small windturbine's owner want a visualization tool to display wind ressourcies and power production. Because we are building our windturbine, this tool can help after a fresh installation to calibrate the tail's regulation by data measurement.
 Puisque nous construisons nos éoliennes, cela permet aussi lors d'une mise en service d'une nouvelle éolienne, de vérifier son bon fonctionnement, est-ce que l'éolienne produit trop ou assez? Est-ce que la régulation fonctionne correctement?

So, we chose a free and modular solution to cover configurations. That permit at everyone to help on the development or modify it for his own needs.

#### Contributors

 * LONGUET Gilles, development/maintener(@gilou_)
 * Luiz Villa, development(@luiz.villa)
 * DAME David, contributor
 * HEMBERT Brice, contributor
 * BENHAMED Sofiane, contributor
 * GILBERT Aurélie, Documentation(@aurelieguibert)

### Partners

<table>
 <tr>
  <td style="max-width: 120px"> <img src="images/logo_aleea.png" alt="logo aleea" style="min-width: 100px"> </td>
  <td>Based near Toulouse (France), ALEEA association's goal is to search, develop, build and promote power electronic for self-producted energies.
  It defend the freedom to use, study, copy, modify and buid electronics system who it develop in a "Hardware spirit" as it's defined on the gnu.org website.
  ALEEA develop knowledges and skills sharing workshops. There goals is to demistify power electronics and energies self-production to peoples.</td>
 </tr>
 <tr>
  <td style="min-width: 120px"><img src="images/logo_tripalium.png" alt="logo association tripalium" style="min-width: 100px"></td>
  <td> Tripalium is an association whose objective is to promote small wind power through the construction of Piggott design small wind turbines in France. Its members contribute to the overall improvement of the Piggott design mainly through their manual which is collectively built and  improved. Members also contribute to solve issues in off-grid electric system through the construction of open-source charge controllers.  Other members are interested in maintenance and estimating the production of their installations, which has created the need for an open-source
  data logger. This has driven the contribution of its members, most notably Gilles Longuet, to the Wind Empowerment Measurement WG.</td>
 </tr>
 <tr>
  <td style="min-width: 120px"><img src="images/logo_WindEmpowerment.png" alt="logo association WindEmpowerment" style="min-width: 100px"></td>
  <td>WindEmpowerment is an association for the development of locally manufactured small wind turbines for sustainable rural electrification. We represent dozens of member organisations, consisting of wind turbine manufacturers, non-governmental organisations, universities, social enterprises, co-operatives, training centres, as well as over 1,000 individual participants across the world.</td>
 </tr>
</table>
</br>

### How much does it cost?
The kit contain the two boards, **Digital** and **Shield**, one set of power measurement sensors and the case. contact : contact@aleea.org

### How does it work?
#### Electrical installation reminder

!["Schéma d'un site isolé par Eve Lomenech"](images/Sch_site_isole_auteure_eve_lomenech.png "Schéma d'un site isolé par Eve Lomenech")


##### Generator
Produce alternative current with a variable freauency, depending from the wind speed. This variable current can't be use directly, it's need to process it.

##### Brake
It's used to stop the wind turbine by connecting the 3 phases from the generator, together in short-circuit. We use it for maintenance and tower manipulation.

##### Diodes rectifier
It convert alternative current to direct current. We can connect the generator before it and the batteries, after.

##### Charging Batteries Controller
In off grid configuration, is protecting the batteries. When they are full, the controller divert the current producted by the windturbine on the dump load.

In on-grid configuration, is protecting the AC converter by diverting over voltage on the dump load. Is protecting the windturbine by charging it when a grid cut off come through.

##### AC converter
Transform the DC current in aternative curent (230V/50Hz or 110V/60Hz). This AC current can be used directly or injected on the grid.

More details in the [Tripalium's manual](https://www.tripalium.org/manuel).

### Install the windlogger

![Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert](images/Sch_site_isole_capteurs_auteure_eve_lomenech.png "Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert")

The better place to install the windlogger is near the power installation. It's simpler to pull wire from wind sensors.

It's possible to measure 4 powers. On an off-grid system, for example, it's possible to measure batteries  DC voltage, DC currents from windvane and solar panels, DC currents consumed by DC charge and AC converter. On an on-grid system, it's possible to measure grid AC voltage, currents from windturbine's inverter and solar panel's inverter and  the grid current's flow.

It's possible to measure in the same time, 2 wind speed and the wind direction.

### Hardware parts

!["The windloger"](images/windlogger.svg "The windlogger")

#### The Digital board
The *digital* board organise the measurement process, record the data and is able to transmit them by USB, Wifi or GPRS. Basic kit come with USB and SD card capabilities.

**Ask for Wifi or GPRS because it's not fully fonctionnal.**

!["Carte digital"](images/board_digital.png "Digital board")

#### The Shield board

The shield board allows the formatting of signals, coming from different sensors, so that they are received correctly by a micro-controller. The sensors are used to take measurements on a wind turbine system. It is part of the [*Windlogger*](https://gitlab.com/gilou_/windlogger/) project.

Like the rest of the project, it was developed to be assembled,
parameterized and used by the maximum number of people. The majority of
components are through, which facilitates the welding of components.

It is mounted on the [*digital*](https://gitlab.com/gilou_/windlogger_digital/) board to form a data logger for small wind.

!["Carte shield"](images/board_shield.png "Shield board")

### Microcontroller's firmware
The firmware is developed to be compatible with Arduino and PlatforIO toolchains. That give it modularity and possible enhancements.

### Software data processing
This is the data analys part. For the moment, no solution is fixed but data are avaiable by transmittion or in CSV format. Everyone can process them as he want.

We are discuting about create a Python's Library to process data in order to have differents visualizations and simple analys. That will permit to develop a web service and a local software based on the same library.

### Sensors

#### Anemometers and windvane
It's possible to connect 2 anemometers and a windvane to the windlogger. With small modification you can install a RPM sensor on one of the anemometer input.

##### Which anemometer?
You can find two anemometer's family. The first are hall effet and give a sinusoidal output. The second work as a simple switch.

These models are working pretty well with de windlogger :

 - NRGsystems :
 - - [NRG40C](https://www.nrgsystems.com/products/met-sensors/anemometers/detail/40c-anemometer)
 - - [NRG40H](https://www.nrgsystems.com/products/met-sensors/anemometers/detail/40h-anemometer-hall-effect)
 - Inspeed Vortex :
 - - [Inspeed Vortex II](http://inspeed.com/anemometers/Vortex_II_Wind_Sensor.asp)
 - - [Inspeed Vortex](http://inspeed.com/anemometers/Vortex_Wind_Sensor.asp)

##### Which windvane
A windvane work as a potentiometer, so the two external outputs give you the full range and the middle output give you the angle.

These models are working pretty well with de windlogger :
 - [NRG200P](https://www.nrgsystems.com/products/met-sensors/wind-direction-vanes/detail/200p-wind-vane)
 - [Inspeed E-Vane](http://inspeed.com/wind_speed_direction/Vane.asp)

##### How to install them?
If you want to install these sensors on an measurement tower, try to put them as the future wind turbine will be, same height, same location...

If your wind turbine is already installed, use a bar to fixe the sensors under the blade's height on the tower.

![environmental sensors](images/environnement_s.svg)

##### How to connect them?
The anemometers (connectors Anemo1 and Anemo2), you need to select with the jumpers on the *shield* board which kind of sensor is.

For hall effect type put jumpers hight or for the others working as switch, put them low :

![Anemometer's jumpers](images/anemo_jumper.png)

Pins :
Effet hall : 1 and 2,
ILS : 1,2,3

For the windvane (connector vane), connect the external outpus to 1 and 3 and the middle one to the pin 2.

#### Power sensors
You can have a DC and a AC voltages measured by the windlogger.

##### Which sensors?
 - DC voltage

Two simple wires connected to the Vdc connector and the pin 1 and 2.

 - AC voltage

[9Vac, ac/ac transformer](https://shop.openenergymonitor.com/ac-ac-power-supply-adapter-ac-voltage-sensor-euro-plug/)

! It's important to use a transformer to reduce the voltage on the board to prevent electrical damage for humain and material.

##### DC current sensor
http://www.europowercomponents.com/media/uploads/HASS50.pdf

Transductor DC sensor type.


##### AC current sensor
[SCT013 sensor familly](https://shop.openenergymonitor.com/100a-max-clip-on-current-sensor-ct/)

##### How to install them?
##### DC voltage
You can connect them to the battery voltage. Use a fuse to prevent dommage.

On the windlogger, connect it on the Vdc connector. Connect the ground on the pin 1 and the Vbat to the pin 2.

##### AC voltage
Connect to the barrel jack the AC round plug from the transformer and  the transformer in the grid.

##### DC current
To monitor battery current, connect the DC sensor on one of the four current connector. See on the datasheet to know how to connect it on the windlogger.

##### AC current
Same as DC current sensor.

## Assembly instructions
